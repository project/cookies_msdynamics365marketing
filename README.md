# COOKiES MS Dynamics365 Marketing Anonymize
*cookies_msdynamics365marketing*

## Functionality
This module neither adds the MS Dynamics 365 Marketing Forms code
to the project, nor it provides functionality to handle the codes.

It simply adds the "anonymize: true" / "anonymize: false" flag JS Snippet from
https://docs.microsoft.com/en-us/dynamics365/marketing/cookies#how-to-disable-non-essential-dynamics-365-marketing-cookies
based on the consent decision.

- If conset IS NOT given (default), it adds "anonymize: true".
- If consent IS given, it adds "anonymize: false".

## How to use
- Enable and configure COOKiES module and this module.
- Add the MS Dynamics 365 Marketing Forms to your page like before.
- Ensure the anonymization is added to every page and is working as expected!
