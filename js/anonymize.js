/**
 * @file
 * Defines Javascript behaviors for the cookies_msdynamics365marketing module.
 */
(function (Drupal) {
  Drupal.behaviors.cookies_msdynamics365marketing_anonymize = {
    // id corresponding to the cookies_service.schema->id.
    id: "msdynamics365marketing",

    activate(context) {
      // Consent given:
      // UN-ANONYMIZE!
      window.d365mktConfigureTracking = function d365mktConfigureTracking() {
        return { Anonymize: false };
      };
      if (window.MsCrmMkt) {
        window.MsCrmMkt.reconfigureTracking({ Anonymize: false });
      }
    },

    fallback(context) {
      // Consent denied / revoked:
      // ANONYMIZE!
      window.d365mktConfigureTracking = function d365mktConfigureTracking() {
        return { Anonymize: true };
      };
      if (window.MsCrmMkt) {
        window.MsCrmMkt.reconfigureTracking({ Anonymize: true });
      }
    },

    attach(context) {
      const self = this;
      document.addEventListener("cookiesjsrUserConsent", function (event) {
        const service =
          typeof event.detail.services === "object"
            ? event.detail.services
            : {};
        if (typeof service[self.id] !== "undefined" && service[self.id]) {
          // Consent given. Allow cookies to be used:
          self.activate(context);
        } else {
          // Consent denied / revoked. Disable cookies:
          self.fallback(context);
        }
      });
    },
  };
})(Drupal);
